﻿using UnityEngine;

namespace SceneObjects {
    public class SoundController : MonoBehaviour {

        [SerializeField] AudioClip cardClick;
        [SerializeField] AudioClip cardMatch;
        [SerializeField] AudioClip win;
        AudioSource _source;
        void Awake() {
            _source = GetComponent<AudioSource>();
        }

        public void PlayClickSound() {
            _source.PlayOneShot(cardClick);
        }

        public void PlayMatchSound() {
            _source.PlayOneShot(cardMatch);
        }

        public void PlayWinSound() {
            _source.PlayOneShot(win);
        }
    }
}
