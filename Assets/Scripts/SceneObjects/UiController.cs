﻿using Models;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace SceneObjects {
    public class UiController : MonoBehaviour
    {
        [SerializeField] GameObject mainPanel;
        [SerializeField] GameObject gamePanel;
        [SerializeField] TextMeshProUGUI finishText;
        public Button startButton;
        public Button playSavedGame;
        public Button saveGame;
        public TextMeshProUGUI bestResult;

        public void StartGame() {
            gamePanel.SetActive(true);
            mainPanel.SetActive(false);
        }

        public void StopGame(bool isWin) {
            finishText.text = isWin ? "You won" : "Game over";
            finishText.gameObject.SetActive(true);
            gamePanel.SetActive(false);
            mainPanel.SetActive(true);
        }

        public void UpdateMainMenu(ValueOrError<GameState> savedGame, ValueOrError<float> bestTime) {
            playSavedGame.gameObject.SetActive(!savedGame.IsError);
            
            bestResult.gameObject.SetActive(!bestTime.IsError);

            if (!bestTime.IsError)
                bestResult.text = $"{(int) bestTime.Value}s";
        }
    }
}
