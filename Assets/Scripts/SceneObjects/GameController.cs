﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Models;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

namespace SceneObjects {
    [RequireComponent(typeof(UiController))]
    [RequireComponent(typeof(DatabaseProvider))]
    [RequireComponent(typeof(SoundController))]
    [RequireComponent(typeof(Timer))]
    public class GameController : MonoBehaviour {

        const int GAME_TIME = 30;
        
        [SerializeField] List<Sprite> images;
        [SerializeField] CardController cardPrefab;
        [SerializeField] Transform cardHolder;
        
        Timer _timer;
        UiController _uiController;
        DatabaseProvider _database;
        SoundController _soundController;
  
        readonly List<Sprite> _cardImages = new List<Sprite>();
        readonly List<CardController> _gameCards = new List<CardController>();

        GameState _lastSavedGame;

        void Awake() {
            _timer = GetComponent<Timer>();
            _uiController = GetComponent<UiController>();
            _database = GetComponent<DatabaseProvider>();
            _soundController = GetComponent<SoundController>();
        }

        void Start() {
            SetupCameraSize();
            Init();
            CardController.OnCardOpened += CheckCards;
            CardController.OnCardClicked += () => _soundController.PlayClickSound();
            _uiController.startButton.onClick.AddListener(StartNewGame);
            _uiController.playSavedGame.onClick.AddListener(StartSavedGame);
            _uiController.saveGame.onClick.AddListener(SaveGame);
        }

        void Init() {
            for (var i = 0; i < 16; i++) {
                var card = Instantiate(cardPrefab);
                var posX = i % 4;
                var posY = i / 4;
                card.transform.position = new Vector3(-7.5f + 5f * posX, 10.5f - 7f * posY, 0);
                card.gameObject.SetActive(false);
                card.gameObject.name = i.ToString();
                _gameCards.Add(card);
                _cardImages.Add(images[i / 2]);
            }
            UpdateSavedData();
        }

        async void StartNewGame() {
            ShuffleCards();
            _uiController.StartGame();
            
            foreach (var (index, card) in _gameCards.Select((value, i) => (i, value))) {
                card.SetImage(_cardImages[index]);
            }

            foreach (var card in _gameCards) {
                card.gameObject.SetActive(true);
                card.state = CartState.Close;
                await Task.Delay(50);
            }
            
            _timer.StartTimer(GAME_TIME, () => StopGame(false));
        }

        async void StartSavedGame() {
            _uiController.StartGame();
            
            foreach (var card in _gameCards) {
                card.gameObject.SetActive(true);
                await Task.Delay(50);
            }
            
            foreach (var (index, card) in _gameCards.Select((value, i) => (i, value))) {
                card.SetImage(images[_lastSavedGame.cards[index].imageIndex]);
                card.SetState((CartState) _lastSavedGame.cards[index].state);
            }

            _timer.StartTimer(_lastSavedGame.currentTime, () => StopGame(false));
        }

        async void StopGame(bool isWin) {
            _uiController.StopGame(isWin);
            _uiController.startButton.GetComponentInChildren<TextMeshProUGUI>().text = "Restart";
            _uiController.startButton.gameObject.SetActive(false);
            await Task.WhenAll(_gameCards.Select(card => card.Disable()));
            _uiController.startButton.gameObject.SetActive(true);
            UpdateSavedData();
        }

        void SaveGame() {
            var state = new GameState() {
                currentTime = _timer.CurrentTime,
                cards = new List<Card>()
            };

            foreach (var cardController in _gameCards) {
                state.cards.Add(new Card {
                    state = (int) cardController.state, 
                    imageIndex = images.IndexOf(cardController.cardImage.sprite)
                });
            }
            
            _database.SaveGameState(state);
        }

        void UpdateSavedData() {
            var savedGame = _database.LoadGameState();
            var bestResult = _database.GetBestResult();
            _lastSavedGame = savedGame.Value;
            _uiController.UpdateMainMenu(savedGame, bestResult);
        }

        void CheckCards(CardController newCard) {
            
            var openedCard = _gameCards.Find(card => card.state == CartState.Open);
            if (openedCard == null) {
                newCard.state = CartState.Open;
                return;
            }

            if (openedCard.cardImage.sprite == newCard.cardImage.sprite) {
                openedCard.state = CartState.Locked;
                newCard.state = CartState.Locked;
                _soundController.PlayMatchSound();
                if (_gameCards.Any(card => card.state != CartState.Locked)) return;

                _database.SaveBestResult(GAME_TIME - _timer.CurrentTime);
                StopGame(true);
                _timer.StopTimer();
                _soundController.PlayWinSound();
                
            } else {
                openedCard.CloseCard();
                newCard.CloseCard();
            }
        }

        void SetupCameraSize() {
            var bounds = cardHolder.GetComponent<MeshRenderer>().bounds;
            var screenRatio = Screen.width / (float)Screen.height;
            float targetRatio = bounds.size.x / bounds.size.y;

            Camera.main.orthographicSize = screenRatio >= targetRatio
                ? bounds.size.y / 2
                : bounds.size.y / 2 * (targetRatio / screenRatio);
        }

        void ShuffleCards() {
            for (var i = 0; i < _cardImages.Count; i++) {
                var temp = _cardImages[i];
                var randomIndex = Random.Range(i, _cardImages.Count);
                _cardImages[i] = _cardImages[randomIndex];
                _cardImages[randomIndex] = temp;
            }
        }
    }
}
