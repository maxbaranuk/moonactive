﻿using System;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;

namespace SceneObjects {
    public class Timer : MonoBehaviour
    {
        [SerializeField] TextMeshProUGUI timeIndicator;

        public float CurrentTime => _time;

        float _time;
        bool _isRun;
        
        public async void StartTimer(float time, Action onTimerFinished) {

            timeIndicator.gameObject.SetActive(true);
            _time = time;
            _isRun = true;
            while (_time > 0 && _isRun) {
                _time -= 0.1f;
                timeIndicator.text = ((int) _time).ToString();
                await Task.Delay(100);
            }
            if (_time <= 0)
                onTimerFinished.Invoke();
        }

        public void StopTimer() {
            _isRun = false;
            if (timeIndicator)
                timeIndicator.gameObject.SetActive(false);
        }

        void OnDisable() {
            StopTimer();
        }
    }
}
