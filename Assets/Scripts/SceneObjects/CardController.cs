﻿using System;
using System.Threading.Tasks;
using UnityEngine;

namespace SceneObjects {
    public class CardController : MonoBehaviour {

        public static Action<CardController> OnCardOpened;
        public static Action OnCardClicked;
        
        public SpriteRenderer cardImage;
        public GameObject cardBack;
        
        public CartState state = CartState.Close;
        
        [SerializeField] Animator animator;

        static readonly int Open = Animator.StringToHash("Open");

        public void SetImage(Sprite sprite) {
            cardImage.sprite = sprite;
        }

        public void SetState(CartState state) {
            this.state = state;
            if (state != CartState.Close)
                animator.SetBool(Open, true);
        }

        public void Click() {
            
            switch (state) {
                case CartState.Open:
                    CloseCard();
                    OnCardClicked?.Invoke();
                    break;
                case CartState.Close:
                    OpenCard();
                    OnCardClicked?.Invoke();
                    break;
                case CartState.Locked:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
        
        async void OpenCard(){
            animator.SetBool(Open, true);
            await Task.Delay(500);
            OnCardOpened?.Invoke(this);
        }
        
        public void CloseCard(){
            animator.SetBool(Open, false);
            state = CartState.Close;
        }

        public async Task Disable() {
            CloseCard();
            await Task.Delay(1000);
            gameObject.SetActive(false);
        }
    }

    public enum CartState {
        Open = 0,
        Close = 1, 
        Locked = 2
    }
}
