﻿using DataBase;
using Models;
using Newtonsoft.Json;
using UnityEngine;

namespace SceneObjects {
    public class DatabaseProvider : MonoBehaviour {
        const string SAVED_GAME_STATE = "savedGame";
        const string BEST_RESULT_KEY = "bestResult";

        public DatabaseHolder databaseHolder;

        public void SaveBestResult(float time) {
            var prevTime = databaseHolder.database.LoadFloat(BEST_RESULT_KEY);

            if (prevTime.IsError || time < prevTime.Value)
                databaseHolder.database.SaveData(BEST_RESULT_KEY, time);
        }
        
        public ValueOrError<float> GetBestResult() {
            return databaseHolder.database.LoadFloat(BEST_RESULT_KEY);
        }

        public void SaveGameState(GameState state) {
            var json = JsonConvert.SerializeObject(state);
            databaseHolder.database.SaveData(SAVED_GAME_STATE, json);
        }

        public ValueOrError<GameState> LoadGameState() {
            var json = databaseHolder.database.LoadString(SAVED_GAME_STATE);
            if (json.IsError)
                return ValueOrError<GameState>.CreateFromError("Can't get saved game");
            
            var state = JsonConvert.DeserializeObject<GameState>(json.Value);
            return ValueOrError<GameState>.CreateFromValue(state);
            
        }
    }
}
