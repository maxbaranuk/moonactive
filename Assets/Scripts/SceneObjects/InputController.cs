﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace SceneObjects {
    public class InputController : MonoBehaviour
    {
        void Update()
        {
#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR            
            if (Input.GetTouch(0).phase == TouchPhase.Began)
                CheckClick(Input.GetTouch(0).position);
        
#else
            if (Input.GetMouseButton(0))
                CheckClick(Input.mousePosition);
#endif
        }

        void CheckClick(Vector2 position) {
            if (EventSystem.current.IsPointerOverGameObject())
                return;
            
            var hit = Physics2D.GetRayIntersection(Camera.main.ScreenPointToRay(position));
            if (hit.collider != null) {
                hit.transform.GetComponent<CardController>()?.Click();
            }
        }
    }
}
