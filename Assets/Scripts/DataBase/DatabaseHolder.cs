﻿using UnityEngine;

namespace DataBase {
    public abstract class DatabaseHolder : ScriptableObject {
        
        public IDatabase database;
        
    }
}
