﻿using UnityEngine;

namespace DataBase {
   [CreateAssetMenu(fileName = "PlayerPrefsDatabase", menuName = "PlayerPrefsDatabase", order = 1)]
   public class PlayerPrefsDatabaseHolder : DatabaseHolder
   {
      public PlayerPrefsDatabaseHolder() {
         database = new PlayerPrefsDatabase();
      }
   }
}
