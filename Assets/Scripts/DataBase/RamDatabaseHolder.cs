﻿using UnityEngine;

namespace DataBase {
   [CreateAssetMenu(fileName = "RamDatabase", menuName = "RamDatabase", order = 1)]
   public class RamDatabaseHolder : DatabaseHolder
   {
      public RamDatabaseHolder() {
         database = new RamDatabase();
      }
   }
}
