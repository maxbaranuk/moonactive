using Models;
using UnityEngine;

namespace DataBase {
    public class PlayerPrefsDatabase : IDatabase {
        
        public void SaveData(string key, string data) {
            PlayerPrefs.SetString(key, data);
            PlayerPrefs.Save();
        }

        public void SaveData(string key, int data) {
            PlayerPrefs.SetInt(key, data);
            PlayerPrefs.Save();
        }

        public void SaveData(string key, float data) {
            PlayerPrefs.SetFloat(key, data);
            PlayerPrefs.Save();
        }

        public void SaveData(string key, bool data) {
            PlayerPrefs.SetInt(key, data ? 1 : 0);
            PlayerPrefs.Save();
        }

        public ValueOrError<string> LoadString(string key) {
            return !PlayerPrefs.HasKey(key) 
                ? ValueOrError<string>.CreateFromError("No such key in database") 
                : ValueOrError<string>.CreateFromValue(PlayerPrefs.GetString(key));
        }

        public ValueOrError<int> LoadInt(string key) {
            return !PlayerPrefs.HasKey(key) 
                ? ValueOrError<int>.CreateFromError("No such key in database") 
                : ValueOrError<int>.CreateFromValue(PlayerPrefs.GetInt(key));
        }

        public ValueOrError<float> LoadFloat(string key) {
            return !PlayerPrefs.HasKey(key) 
                ? ValueOrError<float>.CreateFromError("No such key in database") 
                : ValueOrError<float>.CreateFromValue(PlayerPrefs.GetFloat(key));
        }

        public ValueOrError<bool> LoadBool(string key) {
            return !PlayerPrefs.HasKey(key) 
                ? ValueOrError<bool>.CreateFromError("No such key in database") 
                : ValueOrError<bool>.CreateFromValue(PlayerPrefs.GetInt(key) == 1);
        }
    }
}