﻿
using Models;

namespace DataBase {
    public interface IDatabase {
        void SaveData(string key, string data);
        void SaveData(string key, int data);
        void SaveData(string key, float data);
        void SaveData(string key, bool data);

        ValueOrError<string> LoadString(string key);
        ValueOrError<int> LoadInt(string key);
        ValueOrError<float> LoadFloat(string key);
        ValueOrError<bool> LoadBool(string key);
    }
}
