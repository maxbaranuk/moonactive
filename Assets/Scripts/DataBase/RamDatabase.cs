using System.Collections.Generic;
using Models;

namespace DataBase {
    public class RamDatabase : IDatabase {
        readonly Dictionary<string, string> _stringStorage = new Dictionary<string, string>();
        readonly Dictionary<string, int> _intStorage = new Dictionary<string, int>();
        readonly Dictionary<string, float> _floatStorage = new Dictionary<string, float>();
        readonly Dictionary<string, bool> _boolStorage = new Dictionary<string, bool>();

        public void SaveData(string key, string data) {
            if (_stringStorage.ContainsKey(key))
                _stringStorage[key] = data;
            else
                _stringStorage.Add(key, data);
        }

        public void SaveData(string key, int data) {
            if (_intStorage.ContainsKey(key))
                _intStorage[key] = data;
            else
                _intStorage.Add(key, data);
        }

        public void SaveData(string key, float data) {
            if (_floatStorage.ContainsKey(key))
                _floatStorage[key] = data;
            else
                _floatStorage.Add(key, data);
        }

        public void SaveData(string key, bool data) {
            if (_boolStorage.ContainsKey(key))
                _boolStorage[key] = data;
            else
                _boolStorage.Add(key, data);
        }

        public ValueOrError<string> LoadString(string key) {
            return !_stringStorage.ContainsKey(key) 
                ? ValueOrError<string>.CreateFromError("No such key in database") 
                : ValueOrError<string>.CreateFromValue(_stringStorage[key]);
        }

        public ValueOrError<int> LoadInt(string key) {
            return !_intStorage.ContainsKey(key) 
                ? ValueOrError<int>.CreateFromError("No such key in database") 
                : ValueOrError<int>.CreateFromValue(_intStorage[key]);
        }

        public ValueOrError<float> LoadFloat(string key) {
            return !_floatStorage.ContainsKey(key) 
                ? ValueOrError<float>.CreateFromError("No such key in database") 
                : ValueOrError<float>.CreateFromValue(_floatStorage[key]);
        }

        public ValueOrError<bool> LoadBool(string key) {
            return !_boolStorage.ContainsKey(key) 
                ? ValueOrError<bool>.CreateFromError("No such key in database") 
                : ValueOrError<bool>.CreateFromValue(_boolStorage[key]);
        }
    }
}