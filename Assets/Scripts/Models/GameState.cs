using System;
using System.Collections.Generic;

namespace Models {
    
    [Serializable]
    public class GameState {
        public float currentTime;
        public List<Card> cards;
    }
}