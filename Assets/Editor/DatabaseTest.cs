﻿using DataBase;
using NUnit.Framework;

namespace Tests
{
    public class DatabaseTest {
        const string STRING_KEY = "stringKey";
        const string INT_KEY = "intKey";
        const string FLOAT_KEY = "floatKey";
        const string BOOL_KEY = "boolKey";

        readonly IDatabase _playerPrefsDatabase = new PlayerPrefsDatabase();
        readonly IDatabase _ramDatabase = new RamDatabase();

        [Test]
        public void AddStringToPrefsDatabase()
        {
            _playerPrefsDatabase.SaveData(STRING_KEY, "some String Data");
            var data = _playerPrefsDatabase.LoadString(STRING_KEY);
            Assert.AreEqual("some String Data", data);
        }
        
        [Test]
        public void AddIntToPrefsDatabase()
        {
            _playerPrefsDatabase.SaveData(INT_KEY, 5);
            var data = _playerPrefsDatabase.LoadInt(INT_KEY);
            Assert.AreEqual(5, data);
        }
        
        [Test]
        public void AddFloatToPrefsDatabase()
        {
            _playerPrefsDatabase.SaveData(FLOAT_KEY, 8.0f);
            var data = _playerPrefsDatabase.LoadFloat(FLOAT_KEY);
            Assert.AreEqual(8.0f, data);
        }
        
        [Test]
        public void AddBoolToPrefsDatabase()
        {
            _playerPrefsDatabase.SaveData(BOOL_KEY, true);
            var data = _playerPrefsDatabase.LoadBool(BOOL_KEY);
            Assert.AreEqual(true, data);
        }
        
        [Test]
        public void AddStringToRamDatabase()
        {
            _ramDatabase.SaveData(STRING_KEY, "some String Data");
            var data = _ramDatabase.LoadString(STRING_KEY);
            Assert.AreEqual("some String Data", data);
        }
        
        [Test]
        public void AddIntToRamDatabase()
        {
            _ramDatabase.SaveData(INT_KEY, 5);
            var data = _ramDatabase.LoadInt(INT_KEY);
            Assert.AreEqual(5, data);
        }
        
        [Test]
        public void AddFloatToRamDatabase()
        {
            _ramDatabase.SaveData(FLOAT_KEY, 8.0f);
            var data = _ramDatabase.LoadFloat(FLOAT_KEY);
            Assert.AreEqual(8.0f, data);
        }
        
        [Test]
        public void AddBoolToRamDatabase()
        {
            _ramDatabase.SaveData(BOOL_KEY, true);
            var data = _ramDatabase.LoadBool(BOOL_KEY);
            Assert.AreEqual(true, data);
        }
    }
}
